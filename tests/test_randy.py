#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import unittest
import collections
import random
from randy.core import randy, ParamError, _reset_cache, MaxAttemptsError, \
    RandyError
from types import LambdaType
from six import iteritems

def func():
    return random.randint(0, 4)


def rand(func):
    c = [[1, 2, 3, 4, 5], [6, 7, 1, 2, 3], [4, 5, 6, 7, 1], [2, 3, 4, 5, 6],
         [7, 0, 0, 0, 0]]
    r = 0
    while not r:
        r = c[func()][func()]
    return r


class base(unittest.TestCase):
    def _verify(self, c, count):
        print(sorted(c.items()))
        for k, v in iteritems(c):
            c[k] = (float(v) / count) * 100
        print(sorted(c.items()))
        total = sum(c.values())
        self.assertAlmostEqual(total, 100.0, 1)
        print('total %age:', total)
        print('max diff: ', max(c.values()) - min(c.values()))


class test_original(base):
    def test(self, count=1000):
        self._verify(collections.Counter([rand(func) for _ in range(count)]),
                     count)


class test_randy(base):
    def setUp(self):
        self.fn = lambda: random.randint(1, 5)
        _reset_cache()

    def test(self, count=1000):
        r = randy(5, 7, fn=self.fn)
        print(r)
        self._verify(collections.Counter([r() for _ in range(count)]), count)

    def test_get_item_negative_value(self):
        r = randy(5, 7, fn=self.fn)
        self.assertRaises(ParamError, lambda: r[-1])

    def test_get_item_zero_value(self):
        r = randy(5, 7, fn=self.fn)
        print(r)
        r = r[0]
        print(r)

    def test_iter(self):
        r = randy(5, 7, fn=self.fn)
        ii = iter(r)
        for index, i in enumerate(ii):
            print(i)
            if index == 10:
                break

    def test_getitem(self):
        for index, i in enumerate(randy(5, 7, fn=self.fn)):
            print(i)
            if index == 10:
                break

    def test_getitem_invalid(self):
        r = randy(5, 7, fn=self.fn)
        self.assertRaises(ParamError, lambda: r['q'])

    def test_call(self):
        r = randy(5, 7, fn=self.fn)
        for i in range(10):
            i = r()
            print(i)

    def test_invalid_size(self):
        self.assertRaises(ParamError, lambda: randy(0, 7, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(-1, 7, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy('y', 7, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(None, 7, fn=self.fn))

    def test_invalid_o_size(self):
        self.assertRaises(ParamError, lambda: randy(5, 0, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(5, -1, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(5, 'y', fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(5, None, fn=self.fn))

    def test_o_size_too_small(self):
        self.assertRaises(ParamError, lambda: randy(5, 4, fn=self.fn))
        self.assertRaises(ParamError, lambda: randy(5, 5, fn=self.fn))

    def test_o_size_too_big(self):
        randy(5, 25, fn=self.fn)
        self.assertRaises(ParamError, lambda: randy(5, 26, fn=self.fn))

    def test_max_attempts(self):
        r = randy(5, 7, fn=self.fn)
        assert r.max_attempts is None

        r.max_attempts = 8
        assert r.max_attempts == 8

    def test_max_attempts_invalid(self):
        r = randy(5, 7, fn=self.fn)
        assert r.max_attempts is None

        def fa():
            r.max_attempts = None

        def fb():
            r.max_attempts = '0'

        def fc():
            r.max_attempts = 'q'

        r.max_attempts = None
        r.max_attempts = 1

        self.assertRaises(ParamError, lambda: fb())
        self.assertRaises(ParamError, lambda: fc())

    def test_random_func(self):
        r = randy(5, 7, fn=self.fn)
        assert r.random_func is self.fn
        assert isinstance(r.random_func, LambdaType)

        def f():
            return 1

        r.random_func = f
        assert r.random_func is f

    def test_random_func_invalid(self):
        r = randy(5, 7, fn=self.fn)
        assert r.random_func is self.fn

        def fa():
            r.random_func = None

        def fb():
            r.random_func = '0'

        def fc():
            r.random_func = 'q'

        self.assertRaises(ParamError, lambda: fa())
        r.random_func = lambda: 1

        def voo():
            return 2

        r.random_func = voo
        assert r.random_func is voo
        self.assertRaises(ParamError, lambda: fb())
        self.assertRaises(ParamError, lambda: fc())

    def test_maxAttemptsError(self):
        def func():
            raise MaxAttemptsError(3)

        self.assertRaises(MaxAttemptsError, func)
        self.assertRaises(RandyError, func)
        self.assertRaises(ValueError, func)

        try:
            func()
        except MaxAttemptsError as err:
            assert str(err) == 'Failed to get required value after 3 attempts'


if __name__ == '__main__':
    unittest.main()
